# Welcome to my Gitlab page

Here are my sections so far:

---
### [Debian](https://gitlab.com/ph179/debian/-/tree/main?ref_type=heads)

Everything related to my GNU/Linux Debian setup.

---
### [Python](https://gitlab.com/ph179/python/-/tree/main?ref_type=heads)

All my Python files and projects I'm working on or finished.

---
### [Wallpapers](https://gitlab.com/ph179/wallpapers/-/tree/main?ref_type=heads)

My large wallpaper collection, feel free to browse my collection and download what you like:

[Wallpapers](https://gitlab.com/ph179/wallpapers/-/tree/main/img?ref_type=heads)

### My Wallpapers

I'm using 0107.jpg and 0302.jpg on my private computers right now.

Image 0107.jpg:

![Background](img/0107.jpg)

Image 0302.jpg:

![Background](img/0302.jpg)

I'm using 0071.jpg on my computer at work.

Image 0071.jpg:

![Background](img/0071.jpg)

### My Avatar

Image:

![Avatar](tux001.jpg)